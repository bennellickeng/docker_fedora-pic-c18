# Fedora PIC C18 Build Environment

A Fedora based image containing the Microchip C18 compiler for PICs.

The toolchain is installed in `/opt/microchip/mplabc18/[version]`, where `[version]` is dependant on the version of the toolchain installed in the image. In general it's of the form `vM.N`. Since the C18 compiler is no longer supported by Microchip there is unlikely to ever be another release and so [version]  is most likely `v3.47`. The version is included in the Docker image tag name.

To simplify using the toolchain, the `bin` directory of the toolchain is added to the system `PATH` environment variable by a file in `/etc/profile.d`. These files are only sourced for login shells, so you may have to pass `-l` to your shell when invoking it. Bitbucket Pipelines appears to handle this automatically and toolchain is available on the path in Pipelines.
