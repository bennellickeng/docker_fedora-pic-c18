FROM fedora:31

RUN set -o pipefail && \
  dnf update -y && \
  dnf install -y \
    glibc.i686 \
    libstdc++.i686 \
    make \
    git \
    bzip2 \
    scons \
    cmake \
    protobuf-compiler \
    python2-protobuf \
    zip \
    patch

RUN set -o pipefail && \
  curl -Lo /tmp/installer.run https://github.com/Manouchehri/Microchip-C18-Lite/releases/download/v3.47/mplabc18-v3.47-linux-lite-installer.run && \
  chmod +x /tmp/installer.run && \
  /tmp/installer.run --mode unattended && \
  echo "export PATH=/opt/microchip/mplabc18/v3.47/bin/:$PATH" > /etc/profile.d/c18_path.sh
